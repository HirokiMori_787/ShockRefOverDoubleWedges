# ShockWaveReflectionsOverDoubleWedges

二重マッハ反射のケースファイルを元に，坂が2つあるバージョンのケースファイルも作ってみました．

## Getting started

左から衝撃波がやってきて30度の坂で一度マッハ反射し，55度の坂でもう一度反射する場合のケースです．
物理量を変えたりは，後々やってみます．

![](./DoubleWedge30_55.png)